FROM python:3.7.4-buster
WORKDIR /usr/src/app
RUN apt-get update; apt-get install -y curl cron nano ffmpeg
COPY ./requirements.txt .
RUN pip install -r requirements.txt
ADD ./auto_clean_files /etc/cron.d
RUN chmod +x /etc/cron.d/auto_clean_files
ENV VISUAL=nano