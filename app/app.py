import ptvsd
try:
    ptvsd.enable_attach(address=('0.0.0.0', 4000))
    # ptvsd.wait_for_attach()
except Exception as ex:
    print('ptvsd is not working: ', ex)


from tinydb import TinyDB, Query
import uuid
import json
import os
import logging
import ntpath
from datetime import datetime
from subprocess import call, check_output
from flask import Flask, render_template, send_file, jsonify, g, request
from logging.handlers import RotatingFileHandler



def init_logger(app):
    # initialize the log handler
    logHandler = RotatingFileHandler('report.log', maxBytes=1000, backupCount=1)
    # set the log handler level
    logHandler.setLevel(logging.INFO)
    # set the app logger level
    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(logHandler)


def init_database(app):
    try:
        db_path = f"{app.config['BASE_DIR']}/utu3_db.json"
        return TinyDB(db_path)
    except Exception as e:
        raise e


def report_log(video_code, message=""):
    report_ts = datetime.utcnow().strftime("%Y/%m/%d %H:%M:%S")
    return f"{report_ts}\t{video_code}\t{message}"


def save_success_video(video_code):
    g.db.insert({'status': "ok",
                'video_code': video_code})


def save_failed_video(video_code):
    g.db.insert({'status': "failed",
                'video_code': video_code})

# ----------------------------------------------------------------------

db = None
app = Flask(__name__)
app.config['BASE_DIR'] = os.getcwd()
with app.app_context():
    init_logger(app)
    db = init_database(app)


@app.before_request
def before_request():
    g.db = db


@app.route('/ping', methods=['GET'])
def ping():
    return jsonify(message='ping pong')


@app.route('/test')
def test():
    video_code = 'ADBe9c1sUmE'
    app.logger.info(report_log(video_code))
    return render_template('error.html',
                            video_code=video_code,
                            message="Something went wrong, you can wait a few minutes and <b><i>retry</i></b>. If this still happends <b><i>report</i></b> this to us.")


@app.route('/separate/<video_code>')
def separate_request(video_code):
    try:
        Video = Query()
        g.db.upsert({'status': "ok",
                    'video_code': video_code,
                    'request_separate': True}, Video.video_code == video_code)
        message = 'Please come back later'
        return render_template('request_recieved.html', message=message)
    except Exception as e:
        raise e


@app.route('/separate/videos')
def separate_list():
    try:
        Video = Query()
        data = g.db.search(Video.request_separate == True)
        return jsonify(data=data)
    except Exception as e:
        raise e


@app.route('/download/<video_code>', methods=['GET'])
def download_mp3(video_code):
    try:
        is_karaoke = request.args.get("k", False)
        url = 'https://www.youtube.com/watch?v=%s' % (video_code)
        cmd_1 = "youtube-dl --output /usr/src/app/mp3/%%(title)s.%%(ext)s --extract-audio --prefer-ffmpeg --audio-format mp3 %s --restrict-filenames" % (url)
        call_youtubedl = call(cmd_1.split(), shell=False)
        if call_youtubedl == 0:
            cmd_2 = "youtube-dl --get-filename --output '/usr/src/app/mp3/%%(title)s.%%(ext)s' --extract-audio --prefer-ffmpeg --audio-format mp3 %s --restrict-filenames" % (url)
            file_path = str(check_output(cmd_2.split(), shell=False))
            file_path = file_path.replace("'", '') \
                            .replace('b"', '') \
                            .replace('\\n"', '') \
                            .replace('.webm', '.mp3') \
                            .replace('.m4a', '.mp3')
            filename = ntpath.basename(file_path)
            save_success_video(video_code)
            return send_file(file_path,
                            mimetype='audio/mpeg',
                            as_attachment=True)
        else:
            save_failed_video(video_code)
            return render_template('error.html',
                                    video_code=video_code,
                                    message="Something went wrong, you can wait a few minutes and <b><i>retry</i></b>. If this still happends <b><i>report</i></b> this to us.")
    except Exception as e:
        save_failed_video(video_code)
        report_log(video_code, e)
        return render_template('error.html',
                                video_code=video_code,
                                message="Something went wrong, you can wait a few minutes and <b><i>retry</i></b>. If this still happends <b><i>report</i></b> this to us.")


@app.route('/report/<video_code>', methods=["GET"])
def report_video(video_code):
    app.logger.error(f"Unable download video {video_code}")
